
from models import User
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import smtplib
from pas import MY_ADDRESS, PASSWORD


def SendMail(email):
    to = email

    msg = MIMEMultipart()
    msg['Subject'] = 'B7 Преакселератор'
    msg['From'] = MY_ADDRESS
    msg['To'] = to

    html = """\
    <html>
      <body>
        <p>Здравствуйте,<br>
           Вы зарегистрировались в
           <a href="http://www.hoteroke.ru:8080">Преакселераторе B7</a> <br>
           Онлайн-программа самого активного венчурного фонда в Якутии, которая помогает предпринимателям улучшить свою идею, найти команду и сформулировать описание своего бизнеса для встречи с потенциальным инвестором. 
        </p>
      </body>
    </html>
    """
    text = MIMEText(html, "html")
    msg.attach(text)

    s = smtplib.SMTP('smtp.timeweb.ru', 25)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(MY_ADDRESS, PASSWORD)
    s.sendmail(MY_ADDRESS, to, msg.as_string())
    s.quit()
    print('ok')


def SendRejectMail(id):
    jour = Journey.get_by_id(id)
    to = jour.user_id.email

    img_data = open('1.png', 'rb').read()
    msg = MIMEMultipart()
    msg['Subject'] = 'QR CODE'
    msg['From'] = MY_ADDRESS
    msg['To'] = to

    text = MIMEText("Ваша заявка была отклонена")
    msg.attach(text)

    s = smtplib.SMTP('smtp.timeweb.ru', 25)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(MY_ADDRESS, PASSWORD)
    s.sendmail(MY_ADDRESS, to, msg.as_string())
    s.quit()

    print('ok')

