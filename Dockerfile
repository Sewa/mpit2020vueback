FROM python:3
WORKDIR /home
RUN mkdir /home/data
COPY ../mpit2020vueback/requirements.txt ./
RUN pip install -r requirements.txt
COPY hello.cfg ./
COPY *.py ./
CMD [ "python", "/home/run.py" ]
