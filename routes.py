from app import app
import json
from models import User, Question, Answer
from flask import request, jsonify
from utils import SendMail
import ast
import datetime
from playhouse.shortcuts import model_to_dict
import random

a = 1


@app.route('/getquery', methods=['GET', 'POST'])
def reques():
    if request.data:
        print(1)
        dict_str = request.data.decode("UTF-8")
        data = ast.literal_eval(dict_str)
        user = User()
        print(data)
        user.fio = data['form']['name']
        user.phonenumber = data['form']['phone']
        user.email = data['form']['email']
        props = data['props']
        print(user.fio)
        SendMail(user.email)
        data = json.dumps({"status": "ok"})
        return data
    elif request.json:
        print(1)
        data = request.json
        SendMail('hoteroke@gmail.com')
        return data
    else:
        print(1)
        data = json.dumps({"status": "error"})
        SendMail('hoteroke@gmail.com')
        return data


@app.route('/forum', methods=['GET', 'POST'])
def forum():
    questions = []
    for quest in Question.select().dicts():
        questions.append(quest)
    return {'questions': questions}


# Создание нового диалога
@app.route('/newquestion', methods=['GET', 'POST'])
def newquest():
    dict_str = request.data.decode("UTF-8")
    print(dict_str)
    data = ast.literal_eval(dict_str)
    # Поиск существующего или создание нового пользователя
    user, created = User.get_or_create(fio=data['body']['user']['name'], email=data['body']['user']['email'],
                                       phonenumber=data['body']['user']['phone'])
    question, created1 = Question.get_or_create(header=data['body']['question']['header'],
                                                text=data['body']['question']['text'], answers=[''], user_id=user.id)
    return str(question.id)


@app.route('/getquestion', methods=['GET', 'POST'])
def getqeust():
    dict_str = request.data.decode("UTF-8")
    data = ast.literal_eval(dict_str)
    question = []
    answers_id = []
    answers = []
    for i in Question.select().where(Question.id == data['id']).dicts():
        answers_id.append(i['answers'])
        question.append(i)
    for i in Answer.select().dicts():
        if i['id'] in answers_id:
            answers.append(i)
    print(question)
    print(answers)
    return {'question': question, 'answers': answers}


@app.route('/getanswers', methods=['GET', 'POST'])
def getansw():
    dict_str = request.data.decode("UTF-8")
    data = ast.literal_eval(dict_str)
    answers = []
    for i in Answer.select().dicts():
        if i['question_id']== data['id']:
            for j in User.select().where(User.id == i['user_id']).dicts():
                i['user_id'] = j
            answers.append(i)
    print(answers)
    return {'answers': answers}


@app.route('/giveanswers', methods=['GET', 'POST'])
def giveans():
    dict_str = request.data.decode("UTF-8")
    data = ast.literal_eval(dict_str)
    text = data['text']
    answer = Answer()
    answer.text = text
    answer.user_id = 1
    answer.question_id = 1
    answer.save()
    data = json.dumps({"status": "ok"})
    return data