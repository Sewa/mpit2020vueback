from peewee import *
from database import db


class User(Model):
    phonenumber = CharField()
    fio = CharField()
    email = CharField()

    class Meta:
        database = db



class Question(Model):
    user_id = ForeignKeyField(User)
    header = CharField()
    text = CharField()
    answers = CharField()

    class Meta:
        database = db


class Answer(Model):
    question_id = ForeignKeyField(Question)
    user_id = ForeignKeyField(User)
    text = CharField()

    class Meta:
        database = db

db.create_tables([User, Question, Answer])